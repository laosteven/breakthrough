package Plateau;

import java.util.*;

public class Move
{
	public int aC1;
	public int aR1;
	public int aC2;
	public int aR2;

	public Move()
	{
	}

	public Move( int c1, int r1, int c2, int r2 )
	{
		aC1 = c1;
		aR1 = r1;
		aC2 = c2;
		aR2 = r2;
	}

	public Move( int[] m )
	{
		aC1 = m[0];
		aR1 = m[1];
		aC2 = m[2];
		aR2 = m[3];
	}

	public int[] getParsedMove()
	{
		int[] parsedMove = { aC1, aR1, aC2, aR2 };
		return parsedMove;
	}

	public String getRealMove()
	{
		StringBuilder sb = new StringBuilder();
		sb.append(getValue(aC1));
		sb.append(8 - aR1);
		sb.append(getValue(aC2));
		sb.append(8 - aR2);

		return sb.toString();
	}

	private String getValue( int value )
	{
		Map<Integer, String> repParse = initParser();
		try
		{
			return repParse.get(value);
		}
		catch (NullPointerException e)
		{
			// Unrecognized value
		}
		return "Z";
	}

	private Map<Integer, String> initParser()
	{
		Map<Integer, String> val = new HashMap<Integer, String>();
		val.put(0, "A");
		val.put(1, "B");
		val.put(2, "C");
		val.put(3, "D");
		val.put(4, "E");
		val.put(5, "F");
		val.put(6, "G");
		val.put(7, "H");
		return val;
	}

	@Override
	public String toString()
	{
		return getRealMove();
	}
}
