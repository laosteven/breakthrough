package Plateau;

import Algorithme.*;
import Communication.CommServeur;

public class Main
{
	public static void main(String[] args)
	{
		Board board = new Board();
		MoveCheck check = new MoveCheck();
		CommServeur comm = new CommServeur();
		AlphaBeta ab = new AlphaBeta();
		char joueur = 0;

		while (true)
		{
			char tour = comm.getTour();
			if (joueur == 0)
			{
				joueur = tour;
			}

			if (tour == '0')
			{
				// End the game
				break;
			}

			// Debut de la partie en joueur blanc
			if (tour == '1')
			{
				String message = comm.getBoard(comm.getByteBoard());
				board.update(message);

				System.out.println("Nouvelle partie! Vous jouez blanc, entrez votre premier coup : ");

				// Send new move
				send(ab, board, check, comm, joueur);
			}

			// Debut de la partie en joueur Noir
			if (tour == '2')
			{
				System.out.println("Nouvelle partie! Vous jouez noir, attendez le coup des blancs");

				String message = comm.getBoard(comm.getByteBoard());
				board.update(message);
			}

			// Le serveur demande le prochain coup
			// Le message contient aussi le dernier coup joue.
			if (tour == '3')
			{
				String message = comm.getBoard(comm.getByteMove());
				System.out.println("Dernier coup : " + message);

				// Compute new move
				int[] move = check.moveParse(message);
				board.newMove(move);

				// Send new move
				send(ab, board, check, comm, joueur);
			}

			// Le dernier coup est invalide
			if (tour == '4')
			{
				System.out.println("Coup invalide, entrez un nouveau coup : ");
				send(ab, board, check, comm, joueur);
			}
		}
	}

	private static void send(AlphaBeta ab, Board board, MoveCheck check, CommServeur comm, char joueur)
	{
		// String message = comm.askMoveManually();
		System.out.println("Planification du prochain coup : ");
		String message = check.getNext(ab, board, joueur);
		System.out.println(message);
		if (check.isValid(message))
		{
			int[] move = check.moveParse(message);
			if (check.isValid(board, move, joueur))
			{
				board.newMove(move);
				comm.sendMove(message);
			}
		}
	}
}
