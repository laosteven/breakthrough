package Plateau;

public enum White
{
	// x, -y
	NE(-1, -1),
	N(0, -1),
	NW(1, -1);

	private int x;
	private int y;

	White( int xd, int yd )
	{
		x = xd;
		y = yd;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}
}
