package Plateau;

public enum Black
{
	// x, -y
	SW(-1, 1),
	S(0, 1),
	SE(1, 1);

	private int x;
	private int y;

	Black( int xd, int yd )
	{
		x = xd;
		y = yd;
	}

	public int getX()
	{
		return x;
	}

	public int getY()
	{
		return y;
	}
}
