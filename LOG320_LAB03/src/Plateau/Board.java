package Plateau;

import java.io.*;
import java.util.*;

@SuppressWarnings("serial")
public class Board implements Cloneable, Serializable
{

	public final int BOARDSIZE = 8;
	
	private final int VIDE = 0;
	public final int NOIR = 2;
	public final int BLANC = 4;

	private int[][] gameBoard;
	private Map<Integer, String> piece;

	public Board()
	{
		this.gameBoard = new int[BOARDSIZE][BOARDSIZE];
		definePiecePrint();
	}

	private void definePiecePrint()
	{
		piece = new HashMap<Integer, String>();
		piece.put(VIDE, ".");
		piece.put(NOIR, "n");
		piece.put(BLANC, "w");
	}

	public void update( String temp )
	{
		int[][] tempBoard = new int[8][8];
		String[] values = temp.split(" ");
		int x = 0, y = 0;
		for (String lValue : values)
		{
			tempBoard[x][y] = Integer.parseInt(lValue);
			x++;
			if (x == 8)
			{
				x = 0;
				y++;
			}
		}
		update(tempBoard);
	}

	public void update( int[][] newBoard )
	{
		gameBoard = newBoard;
		print();
	}

	public boolean isEmpty( int[] m )
	{
		return gameBoard[m[0]][m[1]] != VIDE && gameBoard[m[2]][m[3]] == VIDE;
	}

	public boolean isOpposite( int[] m )
	{
		return gameBoard[m[2]][m[3]] != gameBoard[m[0]][m[1]];
	}
	
	public boolean isOpposite( int[] m, char joueur)
	{
		switch(joueur)
		{
			case '1':
				return gameBoard[m[0]][m[1]] == BLANC && gameBoard[m[2]][m[3]] == NOIR && m[0] != m[2];
			case '2':
				return gameBoard[m[0]][m[1]] == NOIR && gameBoard[m[2]][m[3]] == BLANC && m[0] != m[2];
			default:
				break;
		}
		return false;
	}

	public boolean isCurrentlyWhite( int[] m )
	{
		return isCurrentlyWhite(m[0], m[1]);
	}

	public boolean isCurrentlyWhite( int x, int y )
	{
		return gameBoard[x][y] == BLANC;
	}

	public boolean isCurrentlyBlack( int[] m )
	{
		return isCurrentlyBlack(m[0], m[1]);
	}

	public boolean isCurrentlyBlack( int x, int y )
	{
		return gameBoard[x][y] == NOIR;
	}

	public void undoMove( Move mo )
	{
		int[] m = mo.getParsedMove();
		gameBoard[m[0]][m[1]] = gameBoard[m[2]][m[3]];
		gameBoard[m[2]][m[3]] = VIDE;
	}

	public void exploreMove( Move mo )
	{
		int[] m = mo.getParsedMove();
		gameBoard[m[2]][m[3]] = gameBoard[m[0]][m[1]];
		gameBoard[m[0]][m[1]] = VIDE;
	}

	public void newMove( int[] m )
	{
		gameBoard[m[2]][m[3]] = gameBoard[m[0]][m[1]];
		gameBoard[m[0]][m[1]] = VIDE;
		print();
	}

	private void print()
	{
		System.out.println(toString());
		System.out.println("================");
	}
	
	@Override
	public String toString()
	{
		StringBuilder sb = new StringBuilder();
		for (int c = 0; c < 8; c++)
		{
			for (int r = 0; r < 8; r++)
			{
				sb.append(piece.get(gameBoard[r][c]) + " ");
			}
			if(c != 7)
			{
				sb.append("\n");
			}
		}
		return sb.toString();
	}
	
	public Board deepClone()
	{
		try
		{
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(baos);
			oos.writeObject(this);

			ByteArrayInputStream bais = new ByteArrayInputStream(baos.toByteArray());
			ObjectInputStream ois = new ObjectInputStream(bais);
			return (Board) ois.readObject();
		} 
		catch (IOException e) 
		{
			return null;
		} 
		catch (ClassNotFoundException e) 
		{
			return null;
		}
	}
}
