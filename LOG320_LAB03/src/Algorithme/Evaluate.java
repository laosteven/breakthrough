package Algorithme;

import Plateau.Board;

public class Evaluate
{
	public int getScore(Board board, char joueur)
	{
		int positive = 0;
		int negative = 0;
		int lost = 0;
		int distanceScore = 0;
		int dim = board.BOARDSIZE;

		double lostMultiplier = 5;
		double distanceMultiplier = 15;
		double pieceMultiplier = 10;

		if (joueur == '1')
		{
			for (int c = 0; c < dim; c++)
			{
				for (int r = 0; r < dim; r++)
				{
					if (board.isCurrentlyWhite(r, c))
					{
						lost = 0;

						if ((r - 1) >= 0 && (c - 1) >= 0 && board.isCurrentlyBlack(r - 1, c - 1))
						{
							lost++;
						}
						else if ((r - 1) >= 0 && (c + 1) < dim && board.isCurrentlyBlack(r - 1, c + 1))
						{
							lost++;
						}

						distanceScore = dim - 1 - r - 1;
						positive += lost * lostMultiplier;
						positive += distanceScore * distanceMultiplier;
						positive += pieceMultiplier;
					}
					else if (board.isCurrentlyBlack(r, c))
					{
						lost = 0;

						if ((r + 1) < dim && (c - 1) >= 0 && board.isCurrentlyWhite(r + 1, c - 1))
						{
							lost++;
						}
						else if ((r + 1) < dim && (c + 1) < dim && board.isCurrentlyWhite(r + 1, c + 1))
						{
							lost++;
						}

						distanceScore = r + 1;
						negative += lost * lostMultiplier;
						negative += distanceScore * distanceMultiplier;
						negative += pieceMultiplier;
					}
				}
			}
		}
		else if (joueur == '2')
		{
			for (int c = 0; c < dim; c++)
			{
				for (int r = 0; r < dim; r++)
				{
					if (board.isCurrentlyBlack(r, c))
					{
						lost = 0;

						if ((r + 1) < dim && (c - 1) >= 0 && board.isCurrentlyWhite(r + 1, c - 1))
						{
							lost++;
						}
						else if ((r + 1) < dim && (c + 1) < dim && board.isCurrentlyWhite(r + 1, c + 1))
						{
							lost++;
						}

						distanceScore = r + 1;
						positive += lost * lostMultiplier;
						positive += distanceScore * distanceMultiplier;
						positive += pieceMultiplier;
					}
					else if (board.isCurrentlyWhite(r, c))
					{
						lost = 0;

						if ((r - 1) >= 0 && (c - 1) >= 0 && board.isCurrentlyBlack(r - 1, c - 1))
						{
							lost++;
						}
						else if ((r - 1) >= 0 && (c + 1) < dim && board.isCurrentlyBlack(r - 1, c + 1))
						{
							lost++;
						}

						distanceScore = dim - 1 - r - 1;
						negative += lost * lostMultiplier;
						negative += distanceScore * distanceMultiplier;
						negative += pieceMultiplier;
					}
				}
			}
		}
		return positive - negative;
	}
}
