package Algorithme;

import java.util.*;

import Plateau.*;

public class AlphaBeta extends MoveCheck
{
	final private int POSSIBLE_MOVES = 1000;
	final private int MAX_DEPTH_MOVE = 10;
	final private int MAX_TIME = 5000;

	private Evaluate eval;
	private long tStart;
	private ArrayList<MoveStrategyScore> moveList;
	private int moveIndex;
	private Move emergency;

	public AlphaBeta()
	{
		super();
	}

	public Move sendBest(Board board, char joueur)
	{
		tStart = System.currentTimeMillis();
		eval = new Evaluate();
		moveIndex = 0;
		moveList = new ArrayList<MoveStrategyScore>();
		emergency = null;

		for (int i = 0; i <= POSSIBLE_MOVES; i++)
		{
			moveList.add(new MoveStrategyScore(new Move(), Integer.MIN_VALUE));
		}

		Board tmp = board.deepClone();
		alphaBeta(tmp, joueur, true, 0, moveList, new Move(), Integer.MIN_VALUE, Integer.MAX_VALUE);

		Move tmpMv = new Move();
		int tmpScore = Integer.MIN_VALUE;
		for (MoveStrategyScore m : moveList)
		{
			if (!m.mv.getRealMove().equals(new Move().getRealMove()) && m.score > tmpScore && isValid(board, m.mv.getParsedMove(), joueur))
			{
				tmpMv = m.mv;
				tmpScore = m.score;
			}
		}
		if (tmpMv.getRealMove().equals(new Move().getRealMove()))
		{
			tmpMv = emergency;
		}
		return tmpMv;
	}

	protected class MoveStrategyScore
	{
		public Move mv;
		public int score;

		public MoveStrategyScore(Move m, int point)
		{
			mv = m;
			score = point;
		}

		public void updateScore(Move m, int point)
		{
			mv = m;
			score = point;
		}

		@Override
		public String toString()
		{
			return "\n" + mv.getRealMove() + " : " + score;
		}
	}

	public void alphaBeta(Board board, char joueur, boolean toMaximize, int depth, ArrayList<MoveStrategyScore> moveStack, Move move, int alpha, int beta)
	{
		if (!hasTime())
		{
			return;
		}
		if (depth == MAX_DEPTH_MOVE)
		{
			moveStack.get(moveIndex).updateScore(move, eval.getScore(board, joueur));
			if (moveIndex < POSSIBLE_MOVES - 1)
			{
				moveIndex++;
			}
		}
		else
		{
			int bestScore;
			if (toMaximize)
			{
				bestScore = Integer.MIN_VALUE;
			}
			else
			{
				bestScore = Integer.MAX_VALUE;
			}
			MoveStrategyScore best = moveStack.get(moveIndex);
			MoveStrategyScore next = moveStack.get(moveIndex + 1);

			best.updateScore(move, bestScore);
			if (depth == 0)
			{
				Board tmp = board.deepClone();
				tmp.exploreMove(move);
				alphaBeta(tmp, joueur, !toMaximize, depth + 1, moveStack, move, alpha, beta);

				if (toMaximize && next.score > best.score)
				{
					best.updateScore(move, next.score);
				}
				else if (!toMaximize && next.score < best.score)
				{
					best.updateScore(move, next.score);
				}

				if (updateAlphaBeta(toMaximize, best.score, alpha, beta))
				{
					return;
				}
			}
			else
			{
				Vector<Move> moves = nextMove(board, joueur);
				if (emergency == null)
				{
					emergency = moves.get(0);
				}
				java.util.Collections.shuffle(moves);
				if (moves != null)
				{
					for (int i = 0; i < moves.size(); i++)
					{
						Board tmp = board.deepClone();
						tmp.exploreMove(moves.get(i));
						alphaBeta(tmp, joueur, !toMaximize, depth + 1, moveStack, moves.get(i), alpha, beta);

						if (toMaximize && next.score > best.score)
						{
							best.updateScore(moves.get(i), next.score);
						}
						else if (!toMaximize && next.score < best.score)
						{
							best.updateScore(moves.get(i), next.score);
						}

						if (updateAlphaBeta(toMaximize, best.score, alpha, beta))
						{
							return;
						}
					}
				}
			}
		}
	}

	private boolean hasTime()
	{
		return System.currentTimeMillis() - tStart < MAX_TIME;
	}

	private boolean updateAlphaBeta(boolean toMaximize, int best, int alpha, int beta)
	{
		if (!toMaximize)
		{
			beta = Math.min(best, beta);
			if (best <= alpha || best == Integer.MIN_VALUE)
			{
				return true;
			}
		}
		else
		{
			alpha = Math.max(best, alpha);
			if (best >= beta || best == Integer.MAX_VALUE)
			{
				return true;
			}
		}
		return false;
	}
}
