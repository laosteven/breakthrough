package Algorithme;

import java.util.*;

import Plateau.*;

public class MoveCheck
{
	private final Map<String, Integer> posParse;

	public MoveCheck()
	{
		posParse = new HashMap<String, Integer>();
		posParse.put("A", 0);
		posParse.put("B", 1);
		posParse.put("C", 2);
		posParse.put("D", 3);
		posParse.put("E", 4);
		posParse.put("F", 5);
		posParse.put("G", 6);
		posParse.put("H", 7);
	}

	protected int getValue(String letter)
	{
		try
		{
			return posParse.get(letter);
		}
		catch (NullPointerException e)
		{
			// Unrecognized letter
		}
		return 0;
	}

	public int[] moveParse(String move)
	{
		try
		{
			int c1 = getValue(move.substring(0, 1));
			int r1 = 8 - Integer.parseInt(move.substring(1, 2));
			int c2 = 0;
			int r2 = 0;
			if (move.length() > 4)
			{
				c2 = getValue(move.substring(5, 6));
				r2 = 8 - Integer.parseInt(move.substring(6, 7));
			}
			else
			{
				c2 = getValue(move.substring(2, 3));
				r2 = 8 - Integer.parseInt(move.substring(3, 4));
			}

			int[] parsedMove = { c1, r1, c2, r2 };
			return parsedMove;
		}
		catch (NumberFormatException e)
		{
		}
		return new int[4];
	}

	public boolean isValid(String message)
	{
		return message.length() == 4 || message.length() == 7;
	}

	public boolean isValid(Board board, int[] move, char joueur)
	{
		if (inBounds(move) && correctColor(board, move, joueur) && isDirectionValid(board, move))
		{
			if (board.isEmpty(move) || board.isOpposite(move, joueur))
			{
				return true;
			}
		}
		return false;
	}

	public boolean correctColor(Board board, int[] move, char joueur)
	{
		if (joueur == '1' && board.isCurrentlyBlack(move))
		{
			return false;
		}
		if (joueur == '2' && board.isCurrentlyWhite(move))
		{
			return false;
		}
		return true;
	}

	public boolean inBounds(int[] move)
	{
		// verify no outOfBounds
		for (int value : move)
		{
			if (value < 0 || value >= 8)
			{
				return false;
			}
		}

		// verify x
		if (Math.abs(move[0] - move[2]) > 2 || move[3] - move[1] == 0)
		{
			return false;
		}

		// verify y
		if (Math.abs(move[1] - move[3]) > 2)
		{
			return false;
		}

		return true;
	}

	public boolean isDirectionValid(Board board, int[] move)
	{
		if (board.isCurrentlyBlack(move) && move[3] - move[1] < 0)
		{
			return false;
		}
		if (board.isCurrentlyWhite(move) && move[3] - move[1] > 0)
		{
			return false;
		}
		return true;
	}

	public Vector<Move> nextMove(Board board, char joueur)
	{
		Vector<Move> v = new Vector<Move>();
		for (int c = 0; c < 8; c++)
		{
			for (int r = 0; r < 8; r++)
			{
				// Passer a travers chaque piece
				int[][] moves = moveGenerator(board, joueur, r, c);
				for (int[] move : moves)
				{
					if (!Arrays.equals(move, new int[4]) && isValid(board, move, joueur))
					{
						v.add(new Move(move));
					}
				}
			}
		}
		return v;
	}

	public int[][] moveGenerator(Board board, char joueur, int r1, int c1)
	{
		int r2 = 0, c2 = 0;
		int[][] moves = new int[3][4];

		if (joueur == '1')
		{
			// white
			for (White w : White.values())
			{
				r2 = r1 + w.getX();
				c2 = c1 + w.getY();
				int[] move = { r1, c1, r2, c2 };

				if (isValid(board, move, joueur))
				{
					moves[w.ordinal()] = move;
				}
			}
		}
		else
		{
			// black
			for (Black b : Black.values())
			{
				r2 = r1 + b.getX();
				c2 = c1 + b.getY();
				int[] move = { r1, c1, r2, c2 };
				if (isValid(board, move, joueur))
				{
					moves[b.ordinal()] = move;
				}
			}
		}
		return moves;
	}

	public String getNext(AlphaBeta ab, Board board, char joueur)
	{
		Move m = ab.sendBest(board, joueur);
		if (m != null)
		{
			return m.getRealMove();
		}
		return "";
	}
}
