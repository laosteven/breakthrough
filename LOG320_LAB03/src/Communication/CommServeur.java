package Communication;

import java.io.*;
import java.net.Socket;

public class CommServeur
{
	private final int SIZE_BOARD = 1024;
	private final int SIZE_MOVE = 16;

	private Socket MyClient;
	private BufferedInputStream input;
	private BufferedOutputStream output;
	private BufferedReader console;

	public CommServeur()
	{
		try
		{
			MyClient = new Socket("localhost", 8888);
			input = new BufferedInputStream(MyClient.getInputStream());
			output = new BufferedOutputStream(MyClient.getOutputStream());
			console = new BufferedReader(new InputStreamReader(System.in));
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
	}

	public char getTour()
	{
		try
		{
			return (char) input.read();
		}
		catch (IOException | NullPointerException e)
		{
			System.out.println("Stopping run...");
			return '0';
		}
	}

	public String getBoard(int taille)
	{
		byte[] aBuffer = new byte[taille];
		try
		{
			int size = input.available();
			input.read(aBuffer, 0, size);
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
		return new String(aBuffer).trim();
	}

	public String askMoveManually()
	{
		try
		{
			String line = console.readLine();
			sendMove(line);
			return line;
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
		return "";
	}

	public void sendMove(String move)
	{
		try
		{
			output.write(move.getBytes(), 0, move.length());
			output.flush();
		}
		catch (IOException e)
		{
			System.out.println(e);
		}
	}

	public int getByteBoard()
	{
		return SIZE_BOARD;
	}

	public int getByteMove()
	{
		return SIZE_MOVE;
	}
}
