# Breakthrough AI with Alpha-Beta pruning#
-------

## Summary ##

Build an AI made to play a **[breakthrough](https://en.wikipedia.org/wiki/Breakthrough_(board_game))** game and win against the opponent by using **[Alpha-Beta pruning](https://en.wikipedia.org/wiki/Alpha%E2%80%93beta_pruning)** search algorithm.

## Rules ##

With a pre-made game board, every team have to **build** their own algorithms for:

* Connecting with the game and choose their *side* (either `black` or `white`);
* Generate an array of *legal and possible* moves depending on the search depth;
* Send the best possible move in **under 5 seconds**.

## Additional explanation ##

The game board itself, on every turn, will receive a move similar to this format: `A2B3` or `A2-B3`. When a player sends one move, the next player will receive a message containing the position of all the pegs and must evaluate its next best move to send to the board and vice-versa.

# Objectives #
-------
* Find a way to evaluate board situations and **prioritize** moves over others.
* Place rules to validate generated moves in order to *optimize Alpha-Beta pruning* search time.
* Compete in an **AI tournament**.

# For more information #
-------
Visit the following website: [**Data structure and Algorithms** (LOG320)](https://www.etsmtl.ca/Programmes-Etudes/1er-cycle/Fiche-de-cours?Sigle=log320) [*fr*]